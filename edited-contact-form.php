<?php
/**
 * Plugin Name: Edited Contact Form
 * Description: Simple Contact Form that store submitted information in the database and offers an overview of the saved information in the admin panel.
 * Author: Petyo Lazarov
 * Text Domain: edited-contact-form
 * Version: 1.1.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Create a custom table in the database which is to be used for storing the sent emails.
 */
function edited_contact_form_create_emails_db() {
	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();

	$table_name = $wpdb->prefix . 'edited_contact_form_emails';

	$sql = "CREATE TABLE $table_name (
		id int(11) NOT NULL AUTO_INCREMENT,
		name tinytext NOT NULL,
		email VARCHAR(100) NOT NULL,
		subject VARCHAR(70) NOT NULL,
		PRIMARY KEY (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}
register_activation_hook( __FILE__, 'edited_contact_form_create_emails_db' );

/**
 * Add admin page to the menu.
 */
function edited_contact_form_add_admin_page() {
    // Add top level menu page.
    add_menu_page(
        'Edited Form Admin Panel',
        'Edited Form',
        'manage_options',
        'edited-contact-form',
        'edited_contact_form_admin_page_html',
        'dashicons-email',
        80
    );
}
add_action( 'admin_menu', 'edited_contact_form_add_admin_page');

/**
 * Admin page html callback.
 */
function edited_contact_form_admin_page_html() {

    // Check user capabilities.
    if ( ! current_user_can( 'manage_options' ) ) {
        return;
    }

    // Check if the user have submitted the settings.
	if ( isset( $_GET['settings-updated'] ) ) {
		// Add settings saved message.
		add_settings_error( 'edited_contact_form_messages', 'edited_contact_form_message', __( 'Settings Saved', 'edited-contact-form' ), 'updated' );
	}

	// Show error/update messages.
	settings_errors( 'edited_contact_form_messages' );
  
    // Get the active tab from the $_GET param.
    $default_tab = 'edited-contact-form-emails';

    $tab = isset( $_GET['tab'] ) ? $_GET['tab'] : $default_tab;
  
    ?>
    <div class="wrap">
        <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
        <nav class="nav-tab-wrapper">
            <a href="?page=edited-contact-form&tab=edited-contact-form-emails" class="nav-tab <?php if( 'edited-contact-form-emails' === $tab ) : ?>nav-tab-active<?php endif; ?>">📧 Email Records</a>
            <a href="?page=edited-contact-form&tab=edited-contact-form-settings" class="nav-tab <?php if( 'edited-contact-form-settings' === $tab ) : ?>nav-tab-active<?php endif; ?>">⚙️ Settings</a>
            <a href="?page=edited-contact-form&tab=edited-contact-form-info" class="nav-tab <?php if( 'edited-contact-form-info' === $tab ) : ?>nav-tab-active<?php endif; ?>">📖 Info</a>
        </nav>
  
        <div class="tab-content">
            <?php 
                switch ($tab) {
                    case 'edited-contact-form-settings':
                        edited_contact_form_settings_tab_html();
                        break;
                    case 'edited-contact-form-info':
                        edited_contact_form_info_tab_html();
                        break;
                    default:
                        edited_contact_form_table_with_emails();
                        break;
                }
            ?>
        </div>
    </div>
    <?php
}

/**
 * Settings tab html output.
 */
function edited_contact_form_settings_tab_html() {
	?>
	<div class="wrap">
		<form action="options.php" method="post">
			<?php
			    settings_fields( 'edited-contact-form' );
			    do_settings_sections( 'edited-contact-form' );
			    submit_button( 'Save Settings' );
			?>
		</form>
	</div>
	<?php
}

/**
 * Email Records tab html output.
 */
function edited_contact_form_table_with_emails() {
    global $wpdb;
    $tablename = $wpdb->prefix . 'edited_contact_form_emails';

    $email_results = $wpdb->get_results( "SELECT * FROM $tablename ORDER BY id DESC" );
    
    ?>
    <h2><?php esc_html_e( 'Records of sent emails', 'edited-contact-form' ); ?></h2>
    <?php
    if ( $wpdb->last_error ) {
        echo 'wpdb error: ' . $wpdb->last_error;
        return;
    }
    if( empty( $email_results ) ) {
        ?>
            <p class="description" style="font-style:italic;"><?php esc_html_e( 'There are no entries in the database yet', 'edited-contact-form' ); ?></p>
        <?php
        return;
    }
    ?>
        <table class="widefat striped fixed">
            <thead>
            <tr>
                <th><?php esc_html_e( 'ID', 'edited-contact-form' ); ?></th>
                <th><?php esc_html_e( 'Name', 'edited-contact-form' ); ?></th>
                <th><?php esc_html_e( 'Email', 'edited-contact-form' ); ?></th>
                <th><?php esc_html_e( 'Subject', 'edited-contact-form' ); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ( $email_results as $email_data ) : ?>
                <tr>
                    <td>
                        <?php echo esc_html( $email_data->id ); ?>
                    </td>
                    <td>
                        <?php echo esc_html( $email_data->name ); ?>
                    </td>
                    <td>
                        <?php echo esc_html( $email_data->email ); ?>
                    </td>
                    <td>
                        <?php echo esc_html( $email_data->subject ); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php
}

/**
 * Show more information about the plugin in the admin panel.
 */
function edited_contact_form_info_tab_html(){
    ?>
        <h2><?php esc_html_e( 'How to use Edited Contact Form', 'edited-contact-form' ); ?></h2>
        <?php printf( '<p class="description">' . __( 'To use the form on your site you need to insert this shortcode %s in a page or post', 'edited-contact-form' ) . '</p>',  '<code>[edited-contact-form]</code>' ); ?>
    <?php
}

/**
 * Custom option and settings.
 */
function edited_contact_form_settings_init() {

	// Register a new setting.
	register_setting(
        'edited-contact-form',
        'edited_contact_form_options',
        'edited_contact_form_options_sanitize'
    );

	// Register a new section.
	add_settings_section(
		'edited_contact_form_email_settings',
		__( 'Email Settings', 'edited-contact-form' ),
        'edited_contact_form_email_settings_callback',
		'edited-contact-form'
	);

	// Register a new field.
	add_settings_field(
		'edited_contact_form_email',
		__( 'Email address', 'edited-contact-form' ),
		'edited_contact_form_email_field_cb',
		'edited-contact-form',
		'edited_contact_form_email_settings',
		array(
			'label_for' => 'edited_contact_form_email',
            'class' => 'regular-text ltr',
		)
	);
}
add_action( 'admin_init', 'edited_contact_form_settings_init' );

/**
 * Save options.
 */
function edited_contact_form_options_sanitize( $input ) {
    $sanitary_values = array();

    if ( isset( $input['edited_contact_form_email'] ) ) {
        if( ! empty( $input['edited_contact_form_email'] ) ) {
            $sanitary_values['edited_contact_form_email'] = $input['edited_contact_form_email'];
        }
    }

    return $sanitary_values;
}

/**
 * Email settings section callback function.
 *
 * @param array $args The settings array, defining title, id, callback.
 */
function edited_contact_form_email_settings_callback( $args ) {
    $admin_email = get_option( 'admin_email' );
	?>
        <div id="<?php echo esc_attr( $args['id'] ); ?>" class="settings-description">
	        <p class="description"><?php esc_html_e( 'Write an email address to which emails from users should be sent. By default the administrator\'s email will be used.', 'edited-contact-form' ); ?></p>
	        <p class="description"><?php esc_html_e( 'The current admin email is:', 'edited-contact-form' ); ?> <strong><?php echo esc_html( $admin_email ); ?></strong></p>
        </div>
	<?php
}

/**
 * Email field callback function.
 *
 * @param array $args Array with attributes.
 */
function edited_contact_form_email_field_cb( $args ) {

	// Get the value of the setting we've registered with register_setting().
	$options = get_option( 'edited_contact_form_options' );

	?>
	    <input name="edited_contact_form_options[<?php echo esc_attr( $args['label_for'] ); ?>]" type="email" id="<?php echo esc_attr( $args['label_for'] ); ?>" placeholder="example@domain.com" value="<?php echo isset( $options[ $args['label_for'] ] ) ? $options[ $args['label_for'] ] :  '' ?>" class="<?php echo esc_attr( $args['class'] ); ?>">
	<?php
}


/**
 * Load Bootstrap 5 when using User Form.
 */
function edited_contact_form_load_bs5(){
    wp_enqueue_style( 'bootstrap-5', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css', array(), '5.3.3', 'all' );
    wp_enqueue_script( 'bootstrap-5', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js', array(), '5.3.0', true );
}

/**
 * User Contact form shortcode.
 * 
 * @param array $atts Array with attributes.
 */
function edited_contact_form_shortcode( $atts ) {
    $template_file = plugin_dir_path( __FILE__ ) . 'includes/templates/contact-form-template.php';

    if( is_file( $template_file ) ) {
        add_action( 'wp_enqueue_scripts', 'edited_contact_form_load_bs5' );
        ob_start();
        include $template_file;
        return ob_get_clean();
    }

    return false;
}
add_shortcode( 'edited-contact-form', 'edited_contact_form_shortcode' );

/**
 * Register User Contact form script and styles.
 */
function edited_contact_form_scripts() {

	wp_register_script( 'edited-contact-form', plugin_dir_url( __FILE__ ) . 'public/js/edited-contact-form-public.js', array(), '1.1.0', true );

	wp_localize_script( 'edited-contact-form', 'EDITED_CONTACT_FORM_ASSETS', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'nonce' => wp_create_nonce( '_edited_contact_form_wpnonce' ),
		'strings' => (object) array(
			'success_submit' => __( 'Thank you for your message. It has been sent.', 'edited-contact-form' ),
			'error_message' => __( 'One or more fields have an error. Please check and try again.', 'edited-contact-form' ),
			'general_err_message' => __( 'Something went wrong, please try again later!', 'edited-contact-form' ),
            'loading' => __( 'Loading', 'edited-contact-form' ),
            'submit' => __( 'Submit', 'edited-contact-form' )
		)
	) );
		
 	wp_enqueue_script( 'edited-contact-form' );
}
add_action( 'wp_enqueue_scripts', 'edited_contact_form_scripts' );

/**
 * Custom function to remove extra spaces and padding, and protect from XSS attacks.
 */
function edited_contact_form_test_input( $data ) {
    $data = trim( htmlspecialchars( $data ) );
    return $data;
}

/**
 * Form submission AJAX handler.
 */
function edited_contact_form_ajax_handler(){

	// Security check.
	if ( ! isset( $_POST['edited_contact_form_verif_nonce'] ) ) {
        wp_send_json_error( array( 'message' => 'Verification not available!' ) );
		return;
	}

    if ( ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['edited_contact_form_verif_nonce'] ) ), '_edited_contact_form_wpnonce' ) ) {
        wp_send_json_error( array( 'message' => 'Verification fail!' ) );
        return;
    }

    // Check for data exist.
	if ( ! isset( $_POST['name'] ) || empty( $_POST['name'] ) ) {
        wp_send_json_error( array( 'message' => 'Name not found!' ) );
        return;
	}

	if ( ! isset( $_POST['email'] ) || empty( $_POST['email'] ) ) {
        wp_send_json_error( array( 'message' => 'Email not found!' )  );
        return;
	}

	if ( ! isset( $_POST['subject'] ) || empty( $_POST['subject'] ) ) {
        wp_send_json_error( array( 'message' => 'Subject not found!' ) );
        return;
	}

    // Add Simple validations.
    $name = edited_contact_form_test_input( $_POST['name'] );
    if ( !preg_match("/^[a-zA-Zа-яА-Я-' ]*$/u", $name) ) {
        wp_send_json_error( array( 'message' => 'Only letters and white space allowed for Name!' ) );
        return;
    }

    $email = edited_contact_form_test_input( $_POST['email'] );
    if ( !filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
        wp_send_json_error( array( 'message' => 'Invalid email format!' ) );
        return;
    }

    $subject = edited_contact_form_test_input( $_POST['subject'] );
    $subject_length = strlen( $subject );
    if ( 70 < $subject_length ) {
        wp_send_json_error( array( 'message' => 'Subject is too long! Max allowed length is 70 characters.' ) );
        return;
    }

    $feedback_response = array();
    $feedback_response['message'] = 'Something went wrong, please try again later!';

    global $wpdb;
	$tablename = $wpdb->prefix . 'edited_contact_form_emails';

    $data = array( 'name' => $name, 'email' => $email, 'subject' => $subject );
    $format = array('%s', '%s', '%s');

    $wpdb->insert( $tablename, $data, $format );

    $record_id = $wpdb->insert_id;

    if( false === $record_id ) {
        $recording_fail_reason = 'Something went wrong while inserting data into the database!';

        if ( $wpdb->last_error ) {
            $recording_fail_reason = $wpdb->last_error;
        }

        $feedback_response['recording_status'] = array(
            'success' => false,
            'reason' => $recording_fail_reason
        );
    }

    $recipient = get_option( 'admin_email' );

    $options = get_option( 'edited_contact_form_options' );

    if( isset( $options['edited_contact_form_email'] ) && ! empty( $options['edited_contact_form_email'] ) ) {
        $recipient = $options['edited_contact_form_email'];
    }

    $body = 'You have new submission from Edited Contact Form on your website from ' . $_POST['name'] . '<' . $_POST['email'] . '>.';

    $is_mail_sent = wp_mail( $recipient, $_POST['subject'], $body );

    if( false === $is_mail_sent ) {
  
        $feedback_response['email_status'] = array(
            'success' => false,
            'reason' => 'Something went wrong while sending the email!'
        );
    }

    if( false !== $record_id && false !== $is_mail_sent ) {
        $feedback_response['message'] = 'Email sent and stored in db successfully!';
    } else {
        wp_send_json_error( $feedback_response );
        return;
    }

    wp_send_json_success( $feedback_response );
    die();

}
add_action('wp_ajax_form-submission', 'edited_contact_form_ajax_handler');
add_action('wp_ajax_nopriv_form-submission', 'edited_contact_form_ajax_handler');

<div class="container">
    <div class="card" style="max-width: 400px; margin-left: auto; margin-right: auto;">
        <div class="card-header text-center"><h3><?php esc_html_e( 'Edited Contact Form', 'edited-contact-form' ); ?></h3></div>
        <div class="card-body">
            <form id="edited-contact-form" class="row row-cols-1 g-3 needs-validation" method="POST" novalidate>
                <div class="col">
                    <label for="name" class="form-label"><?php esc_html_e( 'Name', 'edited-contact-form' ); ?></label>
                    <input type="text" class="form-control shadow-none" id="name" name="name" required>
                    <div class="invalid-feedback">
                        <?php esc_html_e( 'Please provide a valid name.', 'edited-contact-form' ); ?>
                    </div>
                </div>
                <div class="col">
                    <label for="email" class="form-label"><?php esc_html_e( 'Email address', 'edited-contact-form' ); ?></label>
                    <input type="email" class="form-control shadow-none" id="email" name="email" required>
                    <div class="invalid-feedback">
                        <?php esc_html_e( 'Please provide a valid email.', 'edited-contact-form' ); ?>
                    </div>
                </div>
                <div class="col">
                    <label for="subject" class="form-label"><?php esc_html_e( 'Subject', 'edited-contact-form' ); ?></label>
                    <input type="text" class="form-control shadow-none" id="subject" name="subject" maxlength="70" required>
                    <div class="invalid-feedback">
                        <?php esc_html_e( 'Please provide a valid subject.', 'edited-contact-form' ); ?>
                    </div>
                </div>
                <div class="col text-center">
                    <button type="submit" class="btn btn-primary"><span class="spinner-border spinner-border-sm submit-btn-loader me-2 d-none" role="status" aria-hidden="true"></span><span class="submit-btn-text"><?php esc_html_e( 'Submit', 'edited-contact-form' ); ?></span></button>
                </div>
                <div class="col"><div class="alert alert-info mb-0 alert-message-box d-none" role="alert">A simple info alert—check it out!</div></div>
            </form>
        </div>
    </div>
</div>
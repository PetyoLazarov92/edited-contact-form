# Edited Contact Form

WordPress Plugin that provides a Simple Contact Form wich stores submitted information in the database, sends an email to the predefined recipient from admin panel settings and offers an overview of the saved information in the admin panel.
## Authors

- Petyo Lazarov [@PetyoLazarov92](https://gitlab.com/PetyoLazarov92)


## Installation

1. Download the plugin as a .zip file.
2. From your WordPress dashboard, choose **Plugins > Add New**.
3. Click **Upload Plugin** at the top of the page.
4. Click **Choose File**, locate the plugin .zip file, then click **Install Now**.
5. After the installation is complete, click Activate Plugin.
## Usage/Examples

To use the form on your site you need to insert this shortcode `[edited-contact-form]` in a page or post.

## Roadmap

Tested with:

- WordPress (6.2.2)

- PHP (8.2.0)

- Google Chrome (114.0.5735.199)


## Known Issues

- If you are using the plugin locally on your machine, for example via `http://localhost/` and do not have a mail server set up, it may not send email to the email address specified by you. In this case, the sent information will be stored in the database and will be available for viewing through the admin panel, and an error message will be displayed to the user.

- Bootstrap 5.3.0 was used to stylize the contact form and the associated visual effects functionality, such as displaying and hiding error messages.

  - It is possible to get a conflict with the current site styling  
  - Аlso if the CDN links fail to load the files, the original look of the form may break.
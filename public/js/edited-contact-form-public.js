document.addEventListener('DOMContentLoaded', function () {
    const contactForm = document.querySelector('#edited-contact-form');

    if (null !== contactForm) {

        const contactFormProcessing = {
            btnLoader: contactForm.querySelector('.submit-btn-loader'),
            btnText: contactForm.querySelector('.submit-btn-text'),
            alertBox: contactForm.querySelector('.alert-message-box'),
            submitBtn: contactForm.querySelector('[type="submit"]'),
            allFields: contactForm.querySelectorAll('input'),
            onSubmit: function (ev) {
                ev.preventDefault();
                ev.stopPropagation();

                if (this.checkValidity()) {
                    // Validation pass
                    contactFormProcessing.loading.start();
                    const formData = new FormData(this);

                    contactFormProcessing.submitForm(formData);
                }

                this.classList.add('was-validated');
            },
            submitForm: function (data) {
                data.append('action', 'form-submission');
                data.append('edited_contact_form_verif_nonce', EDITED_CONTACT_FORM_ASSETS.nonce);

                fetch(EDITED_CONTACT_FORM_ASSETS.ajaxurl, {
                    method: 'POST',
                    body: data,
                }).then((res) => res.json()).then((responseData) => {
                    this.loading.end();
                    if (true === responseData.success) {
                        // Clear inputs, enable button, return success feedback.
                        this.showFeedback('success', EDITED_CONTACT_FORM_ASSETS.strings.success_submit);
                        this.clearForm();
                    }

                    if (false === responseData.success) {
                        // Some Error during form submission processing in the backend.
                        this.showFeedback('error', responseData.data?.message);
                    }
                }).catch(err => {
                    this.loading.end();
                    this.showFeedback('error', EDITED_CONTACT_FORM_ASSETS.strings.general_err_message);
                    console.log(err);
                })
            },
            loading: {
                start: function () {
                    contactFormProcessing.submitBtn.disabled = true;
                    contactFormProcessing.btnText = EDITED_CONTACT_FORM_ASSETS.strings.loading;
                    contactFormProcessing.btnLoader.classList.remove('d-none');
                    contactFormProcessing.alertBox.classList.add('d-none');
                },
                end: function () {
                    contactFormProcessing.btnLoader.classList.add('d-none')
                    contactFormProcessing.btnText = EDITED_CONTACT_FORM_ASSETS.strings.submit;
                    contactFormProcessing.submitBtn.disabled = false;
                }
            },
            clearForm: function () {
                this.allFields.forEach(input => input.value = '');
                contactForm.classList.remove('was-validated');
            },
            showFeedback: function (type = 'info', message = '') {
                this.alertBox.classList.remove('alert-info', 'alert-danger', 'alert-success');
                switch (type) {
                    case 'success':
                        this.alertBox.classList.add('alert-success');
                        break;
                    case 'error':
                        this.alertBox.classList.add('alert-danger');
                        break;
                    case 'info':
                    default:
                        this.alertBox.classList.add('alert-info');
                        break;
                }
                this.alertBox.innerText = message;
                this.alertBox.classList.remove('d-none');
            }
        }
        contactForm.addEventListener('submit', contactFormProcessing.onSubmit)
    }
});